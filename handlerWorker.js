const SubscriptionsGateway = require('./modules/subscriptions/SubscriptionsGateway');

const consumer = async (event) => {
  for (const record of event.Records) {
    const obj = JSON.parse(record.body);

    const createResponse = await SubscriptionsGateway.createSubscription(obj);
    if (!createResponse) {
      throw new Error('create subscription error');
    }

    const responseSubscriptions = await SubscriptionsGateway.requestSubscription(obj.action, obj.subscriptionId, obj.subscriptionData);
    const status = responseSubscriptions ? 'sent' : 'error';

    const updateResponse = await SubscriptionsGateway.updateSubscription(createResponse._id, status);
    if (!updateResponse) {
      throw new Error('update subscription error');
    }
  }
};

module.exports = {
  consumer,
};
