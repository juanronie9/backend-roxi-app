const express = require('express');
const serverless = require('serverless-http');
const SubscriptionsRouter = require('./api/subscriptions/SubscriptionsRouter');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', SubscriptionsRouter);

app.use((req, res, next) => {
  return res.status(404).json({
    error: 'Not Found',
  });
});

module.exports.handler = serverless(app);
