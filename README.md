# Serverless Framework - CRM Events (DynamoDB, SQS, ExpressJS, AWS)

This template demonstrates how to develop and deploy a simple Node Express API service, backed by DynamoDB database and SQS running on AWS Lambda using the traditional Serverless Framework.

## Usage

### Deployment

This example is made to work with the Serverless Framework dashboard, which includes advanced features such as CI/CD, monitoring, metrics, etc.

In order to deploy with dashboard, you need to first login with:

```
serverless login
```

install dependencies with:

```
npm install
```

and then perform deployment with:

```
serverless deploy --region eu-west-1
```

### Remove Services
```
serverless remove --region eu-west-1
```

### API DOCS

#### Post webhook event

`POST /webhook-receive`

**Params:**

| Parameter      | Type    | Description                  | IsRequired | Location |
| -------------- | ------- | ---------------------------- | ---------- | -------- |
| `timestamp`    | String  |ISO8601; e.g. “2021-03-16T10:26:00.000Z” | true       | body    |
| `action`       | String  | ‘PURCHASE’, ‘RENEW’, ‘CANCEL’| true       | body    |
| `subscription_id`| String | The subscription identifier | true       | body    |
| `subscription_data?` | Any | will be present for ‘PURCHASE’ and ‘RENEW’ actions and absent for ‘CANCEL’ | false      | body    |

**Example**:

```bash
curl -X POST \
  https://xxxxxx.eu-west-1.amazonaws.com/dev/webhook-receive \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 05a766e6-0ca7-599e-8546-5dd911db7920' \
  -d '{ 
	"timestamp": "2021-03-16T10:26:00.000Z",
	"action": "PURCHASE",
	"subscription_id": "12345",
	"subscription_data": "subscription 12345"
}'
```
**Response:**
```bash
{
    "message": "OK"
}
```

### TESTS
Run tests in terminal
```bash
npm run test
```