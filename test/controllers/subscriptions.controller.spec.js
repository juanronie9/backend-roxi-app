const request = require('supertest');
const express = require('express');
const _ = require('lodash');
const SubscriptionsRouter = require('../../api/subscriptions/SubscriptionsRouter');
const SubscriptionsGateway = require('../../modules/subscriptions/SubscriptionsGateway');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/', SubscriptionsRouter);

jest.spyOn(SubscriptionsGateway, 'produceSubscription').mockReturnValue(true);

const bodyRequest = {
  timestamp: '2021-03-16T10:26:00.000Z',
  action: 'PURCHASE',
  subscription_id: '12345',
  subscription_data: 'subscription data',
};

describe('Create Subscriptions [POST] /webhook-receive', () => {
  it('should fail when request parameters are not valid: missing required parameters', async () => {
    const response = await request(app).post('/webhook-receive').send({});

    expect(response.statusCode).toBe(400);
    const { body } = response;
    expect(body.errors.length).toBeGreaterThanOrEqual(1);
  });

  it('should fail when request parameters are not valid: timestamp parameter type', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    bodyRequestTest.timestamp = '---';
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(400);
    const { body } = response;
    expect(body.errors.length).toBeGreaterThanOrEqual(1);
  });

  it('should fail when request parameters are not valid: action parameter value', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    bodyRequestTest.action = 'test';
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(400);
    const { body } = response;
    expect(body.errors.length).toBeGreaterThanOrEqual(1);
  });

  it('should fail when request parameters are not valid: action=PURCHASE and missing subscription_data', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    delete bodyRequestTest.subscription_data;
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(400);
    const { body } = response;
    expect(body.errors.length).toBeGreaterThanOrEqual(1);
    expect(body.errors[0].message).toBe('subscriptions_data parameter required');
  });

  it('should fail when request parameters are not valid: action=PURCHASE and subscription_data empty', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    bodyRequestTest.subscription_data = '';
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(400);
    const { body } = response;
    expect(body.errors.length).toBeGreaterThanOrEqual(1);
  });

  it('should fail when request parameters are not valid: action=CANCEL and subscription_data', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    bodyRequestTest.action = 'CANCEL';
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(400);
    const { body } = response;
    expect(body.errors.length).toBeGreaterThanOrEqual(1);
  });

  it('should success: action=PURCHARSE', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(201);
    const { body } = response;
    expect(body.message).toBe('OK');
  });

  it('should success: action=RENEW', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    bodyRequestTest.action = 'RENEW';
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(200);
    const { body } = response;
    expect(body.message).toBe('OK');
  });

  it('should success: action=CANCEL', async () => {
    const bodyRequestTest = _.clone(bodyRequest);
    bodyRequestTest.action = 'CANCEL';
    delete bodyRequestTest.subscription_data;
    const response = await request(app).post('/webhook-receive').send(bodyRequestTest);

    expect(response.statusCode).toBe(202);
    const { body } = response;
    expect(body.message).toBe('OK');
  });
});
