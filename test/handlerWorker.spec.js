const { v1: uuidv1 } = require('uuid');
const SubscriptionsGateway = require('../modules/subscriptions/SubscriptionsGateway');
const handlerWorker = require('../handlerWorker');

jest.spyOn(SubscriptionsGateway, 'updateSubscription').mockReturnValue(true);
jest.spyOn(SubscriptionsGateway, 'createSubscription').mockReturnValue({
  _id: uuidv1(),
  action: 'PURCHASE',
  subscriptionId: '1234',
  subscriptionData: 'test',
  timestamp: '1633006016',
});

const event = {
  Records: [
    {
      messageId: '2cea8225-a8b0-4d5f-a8a6-57ad7f3fb2ec',
      receiptHandle:
        'AQEBxtIsC1fjSp/yHOY4B0ktamZiPfzsPVlWu3ceTrLcSBitfmbBUonDl9LQv5DgQFeoiWg/yESkvPoLvWxn4v0y4BHwwd8PkkSDMIzbdxZ49K7fI+ecJ+SuLl+SRdJSLpXCuXnQiiTJM2Q8SU6st4VmC+pqXuPBWBiYPz4OriZAuALlNt4BmID58Mb2h8xm+zD7+BjqvkWTK7qNgblXsD+Zct7nB3wIAT4AUUz/qVAuQZh1hx5Y0gZwRozIl67iO1VQzTZWGX6fIY+KMhe77nBPE1YQUs/3FdVUXKAsYIY8ATCEPpY5S3iOUY58IDNZdF3aLWDjrYAjbuMUSYRyOzFvF4RWpEbOpR1juRpkJF538RaYpj1kfA+esf8Wroxc5FUHrFN6iYutdXh70CBwN+rU1g==',
      body: '{"timestamp":"1633006016","action":"PURCHASE","subscriptionId":"1234","subscriptionData":"test"}',
      attributes: [Object],
      messageAttributes: {},
      md5OfBody: '2dc773750d060098ef5fbc0945664345',
      eventSource: 'aws:sqs',
      eventSourceARN: 'arn:aws:sqs:eu-west-1:787185068071:workerQueue-dev',
      awsRegion: 'eu-west-1',
    },
  ],
};

describe('handlerWorker: consumer(event)', () => {
  it('should success', async () => {
    try {
      await handlerWorker.consumer(event);
      expect(true).toBeTruthy();
    } catch (error) {
      // should not come here
    }
  });

  it('should throw error', async () => {
    jest.spyOn(SubscriptionsGateway, 'updateSubscription').mockReturnValue(false);
    try {
      await handlerWorker.consumer(event);
    } catch (error) {
      expect(error.message).toBe('update subscription error');
    }
  });
});
