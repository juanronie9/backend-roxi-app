const SubscriptionsGateway = require('../../modules/subscriptions/SubscriptionsGateway');

describe('requestSubscription', () => {
  it('requestSubscription', async () => {
    const action = 'RENEW';
    const subscriptionId = '1234';
    const subscriptionData = 'test';
    const response = await SubscriptionsGateway.requestSubscription(action, subscriptionId, subscriptionData);
    expect(response).toBe(false);
  });
});
