const joi = require('@hapi/joi');

const validationSchema = joi.object().keys({
  timestamp: joi
    .date()
    .required(),
  action: joi
      .string()
      .valid('PURCHASE', 'RENEW', 'CANCEL')
      .required(),
  subscription_id: joi
      .string()
      .required(),
  subscription_data: joi
      .any()
      .optional()
});

module.exports = validationSchema;
