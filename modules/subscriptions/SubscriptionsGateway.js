const AWS = require('aws-sdk');
const { v1: uuidv1 } = require('uuid');
const request = require('request');
const actionsEnum = require('../common/actions.enum');

const dynamoDbClient = new AWS.DynamoDB.DocumentClient();
const sqs = new AWS.SQS();
const { SUBSCRIPTIONS_TABLE, SUBSCRIPTIONS_API_URI, QUEUE_URL } = process.env;

/**
 * Adds a Subscription to a DynamoDB table
 * @param {Object} subscription - Object subscription with values of primitive data types
 * @returns {boolean}          Whether the subscription was added to the DB.
 */
async function createSubscription(subscription) {
  try {
    const params = {
      TableName: SUBSCRIPTIONS_TABLE,
      Item: {
        _id: uuidv1(),
        action: subscription.action,
        subscriptionId: subscription.subscriptionId,
        subscriptionData: subscription.subscriptionData,
        timestamp: subscription.timestamp,
      },
    };
    await dynamoDbClient.put(params).promise();
    return params.Item;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Update a Subscription in a DynamoDB table
 * @param {string} subscriptionId - subscriptionId
 * @param {string} status - Object subscription with values of primitive data types
 * @returns {boolean}          Whether the subscription was updated in DB.
 */
async function updateSubscription(subscriptionId, status) {
  try {
    const params = {
      TableName: SUBSCRIPTIONS_TABLE,
      Key: {
        _id: subscriptionId,
      },
      UpdateExpression: 'set crm_status = :status',
      ExpressionAttributeValues: {
        ':status': status,
      },
      ReturnValues: 'UPDATED_NEW',
    };
    await dynamoDbClient.update(params).promise();
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}

/**
 * Request to external API depends on action
 * @param {string} action - action type
 * @param {string} subscriptionId - subscription id value
 * @param {any} subscriptionData - subscription data value
 * @returns {boolean}          Whether the subscription response was success or not.
 */
async function requestSubscription(action, subscriptionId, subscriptionData) {
  let methodApi;
  switch (action) {
    case actionsEnum.RENEW:
      methodApi = 'PUT';
      break;
    case actionsEnum.CANCEL:
      methodApi = 'DELETE';
      break;
    default:
      methodApi = 'POST';
  }

  const uri = `${SUBSCRIPTIONS_API_URI}/subscriptions/v1/${subscriptionId}`;
  const options = {
    method: methodApi,
    uri,
    headers: {
      'Content-type': 'application/json',
      Accept: 'application/json',
    },
    body: {
      subscription_data: subscriptionData,
    },
    json: true,
  };

  const res = await new Promise((resolve, reject) => {
    request(options, (err, response, body) => {
      if (err) {
        console.log('Error during request subscriptions: ', err);
        return resolve(false);
      }

      if (response.statusCode >= 400) {
        console.log(`Unable to request subscriptions. ErrorCode: ${response.statusCode}`);
        return resolve(false);
      }

      return resolve(true);
    });
  });

  return res;
}

/**
 * Send message subscription to SQS
 * @param {Object} obj - Object subscription with values of primitive data types
 * @returns {boolean}          Whether the subscription response was success or not.
 */
async function produceSubscription(obj) {
  try {
    await sqs
      .sendMessage({
        QueueUrl: QUEUE_URL,
        MessageBody: JSON.stringify(obj),
      })
      .promise();
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}

module.exports = {
  createSubscription,
  updateSubscription,
  requestSubscription,
  produceSubscription,
};
