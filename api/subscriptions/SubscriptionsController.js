const actionsEnum = require('../../modules/common/actions.enum');
const SubscriptionsGateway = require('../../modules/subscriptions/SubscriptionsGateway');

class SubscriptionsController {
  static async createSubscription(req, res) {
    try {
      const { timestamp, action, subscription_id: subscriptionId, subscription_data: subscriptionData } = req.body;

      if ((action === actionsEnum.PURCHASE || action === actionsEnum.RENEW) && !subscriptionData) {
        return res.status(400).json({ errors: [{ message: 'subscriptions_data parameter required' }] });
      }
      if (action === actionsEnum.CANCEL && subscriptionData) {
        return res.status(400).json({ errors: [{ message: 'subscriptions_data parameter need to be absent when action=CANCEL' }] });
      }

      const messageBody = {
        timestamp,
        action,
        subscriptionId,
        subscriptionData,
      };
      const result = await SubscriptionsGateway.produceSubscription(messageBody);
      if (!result) {
        return res.status(400).json({ errors: [{ message: 'produceSubscription error sending message to Queue' }] });
      }

      let statusCode;
      switch (action) {
        case actionsEnum.PURCHASE:
          statusCode = 201;
          break;
        case actionsEnum.CANCEL:
          statusCode = 202;
          break;
        default:
          statusCode = 200;
      }
      return res.status(statusCode).json({ message: 'OK' });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ error: 'Could not retreive webhook' });
    }
  }
}

module.exports = SubscriptionsController;
