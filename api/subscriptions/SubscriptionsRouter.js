const { Router } = require('express');
const SubscriptionsController = require('./SubscriptionsController');
const RequestValidator = require('../../lib/RequestValidator');

const validator = RequestValidator.getInstance();
const router = Router();
const createSchema = require('../../modules/webhooks/createSchema');

router.post('/webhook-receive', validator.validateBody(createSchema), SubscriptionsController.createSubscription);

module.exports = router;
